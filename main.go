package main

import "log"
import "net/http"

func main() {
	log.Printf("Application %s starting.", "Test app")

	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello"))
	})

	http.ListenAndServe(":8080", mux)
}
