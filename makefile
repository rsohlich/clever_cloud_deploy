# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test

all: build
build: 
	$(GOBUILD) -o infopool -v
build-linux:
	# CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOBUILD) -o linux_bin -v
	docker run --rm -it -v "$(GOPATH)":/go -w /go/src/bitbucket.org/rsohlich/test golang:latest go build -o bin -v
clean: 
	$(GOCLEAN)
test: 
	$(GOTEST) -v ./...
run:
	$(GOBUILD) -v ./...
	./infopool
psql:
	docker run -it --rm --link postgres:postgres postgres psql -h postgres -U app -d infopool
docker-image:
	docker build --rm -t infopool .
docker-run:
	docker run -it --rm infopool:latest